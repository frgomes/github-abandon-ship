#!/bin/bash


### Install requirements on Debian and Redhat based distributions.
function github_abandonship_requirements {
  local curl=$(which curl)
  local git=$(which git)
  local jq=$(which jq)
  if [ -z "$curl" -o -z "$git" -o -z "$jq" ] ;then
    sudo apt install -y curl jq git
  fi
}


### Retrieves all your repositories on Gitbub.
### Usage: $ abandon_ship_list_repos
function github_abandonship_list_repos {
  # I'm supposing you have up to 500 repositories.
  # I hope that's enough ...
  for hundreds in 1 2 3 4 5 ;do
    if [ ! -f ${work}/repos_${hundreds}.json ] ;then
      curl -s "https://api.github.com/users/frgomes/repos?per_page=100&page=${hundreds}"
    fi
  done
}


### Selects only those repositories which happen to be public.
### Example: $ abandon_ship_list_repos | abandon_ship_select_public
function github_abandonship_select_public {
  jq '.[] | select(.private==false) | .full_name' | sed 's/"//g'
}


### Selects only those repositories which happen to be private.
### Example: $ abandon_ship_list_repos | abandon_ship_select_private
function github_abandonship_select_private {
  jq '.[] | select(.private==true) | .full_name' | sed 's/"//g'
}


### Migrates repositories to Bitbucket, under an [optional] project.
### Example: $ abandon_ship_list_repos | abandon_ship_select_public | abandon_ship_migrate_bitbucket PUBLIC
function github_abandonship_migrate_bitbucket {
  local github_user=frgomes
  ##local bitbucket_user='rgomes@mathminds.io'
  ##local bitbucket_password='@@CodeBucket588235@@##//'
  local bitbucket_project=PUBLIC

  local work=~/tmp/github_abandon_ship
  mkdir -p ${work}
  if [ -d ${work} -a -w ${work} ] ;then
    while read repo ;do
      pushd ${work} > /dev/null 2>&1

      project=$(echo $repo | cut -d/ -f2)

      echo "Reading repository from git@github.com:${repo}.git ..."
      echo git clone --bare git@github.com:${repo}.git
      echo cd ${project}.git

      echo "Creating repositoy git@bitbucket.org:${bitbucket_user}/${project}.git ..."
      echo ## curl POST -v -u "${bitbucket_user}:${bitbucket_password}" -H "Content-Type: application/json" \
      echo curl POST -v -H "Content-Type: application/json" \
           -d '{"scm": "git", "project": {"key":"'${bitbucket_project}'"}}' \
           https://api.bitbucket.org/2.0/repositories/${bitbucket_user}/${project}

      echo "Pushing mirror onto git@bitbucket.org:${bitbucket_user}/${project}.git ..."
      echo git push --mirror git@bitbucket.org:${bitbucket_user}/${project}.git

      cd ${work}
      echo "Removing temporary working copy of git@github.com:${repo}.git ..."
      echo rm -rf ${project}.git

      echo "-----------------------------------------------------------------------------"

      popd > /dev/null 2>&1

      sleep 5
    done
  else
    ( >&2 echo "Couldn't create directory or it is not writeable: ${work}" )
  fi
}


echo Summary of available functions
echo ==============================
cat $(test -L "$BASH_SOURCE" && readlink -f "$BASH_SOURCE" || echo "$BASH_SOURCE") | \
  egrep -e '^###|^function' | awk '{print $0}; /function/ {print "\n"}; '
